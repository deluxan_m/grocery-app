import React from 'react';
import {useSelector} from 'react-redux';

import {Auth} from './src/navigation/stack';

const App = () => {
  const auth = useSelector(state => state.auth);

  return !auth.isLoggedIn && <Auth />;
};

export default App;
