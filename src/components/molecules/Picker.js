import React, {useState} from 'react';
import {View, Modal, TouchableOpacity, StyleSheet} from 'react-native';

import {COLORS, SIZES} from '../../constants';

import {Typography, Icons} from '../atoms';
import {Modals} from '../molecules';

const Picker = ({label, topic, data}) => {
  const [modalVisible, setModalVisible] = useState(false);

  const renderData = () => {
    return data.map((text, index) => (
      <TouchableOpacity
        key={text.id}
        style={[styles.child]}
        onPress={() => setModalVisible(false)}>
        <Typography variant="body3" align="left">
          {text.value}
        </Typography>
      </TouchableOpacity>
    ));
  };

  const renderModal = () => {
    return (
      <View>
        <View style={styles.centeredView}>
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}>
            <View style={styles.centeredView}>
              <View style={styles.modalView}>{renderData()}</View>
            </View>
          </Modal>
        </View>
      </View>
    );
  };

  return (
    <View>
      <View style={styles.container}>
        <Typography
          align="left"
          color={COLORS.darkGray}
          marginBottom={SIZES.margin}>
          {label}
        </Typography>
        <TouchableOpacity
          style={styles.select}
          onPress={() => setModalVisible(true)}>
          <Typography variant="body3" align="left">
            {topic}
          </Typography>
          <Icons variant="Material" icon="keyboard-arrow-down" size={18} />
        </TouchableOpacity>
      </View>
      {renderModal()}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: SIZES.margin,
    marginHorizontal: SIZES.margin * 4,
    paddingBottom: SIZES.padding * 2,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.darkGray,
  },
  select: {
    flexDirection: 'row',
    marginRight: SIZES.margin * 4,
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    width: '80%',
    backgroundColor: 'white',
    borderRadius: 10,
    paddingVertical: SIZES.padding * 3,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 3,
  },
  child: {
    paddingVertical: SIZES.padding * 2,
    paddingHorizontal: SIZES.padding * 4,
    borderBottomColor: COLORS.darkGray,
  },
});

export default Picker;
