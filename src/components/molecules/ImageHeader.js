import React from 'react';
import {Image, View, StyleSheet} from 'react-native';

import {COLORS, SIZES} from '../../constants';

import {Typography} from '../atoms';

const ImageHeader = props => {
  const {image, title, subtitle} = props;

  return (
    <>
      <View style={styles.container}>
        <Image source={image} />
      </View>
      <View style={styles.textContainer}>
        <Typography align="left" variant="h6">
          {title}
        </Typography>
        <Typography
          align="left"
          color={COLORS.darkGray}
          marginTop={SIZES.margin * 2}>
          {subtitle}
        </Typography>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.white,
    height: 250,
  },
  textContainer: {
    marginHorizontal: SIZES.margin * 4,
  },
});

export default ImageHeader;
