import React from 'react';
import {TouchableOpacity, StyleSheet, View} from 'react-native';

import {COLORS, SIZES} from '../../constants';
import {Typography, Icons} from '../atoms';

const Buttons = props => {
  const {onPress, color, fullWidth, children, icon, iconColor, style} = props;

  return (
    <TouchableOpacity
      onPress={onPress}
      style={[
        styles.button,
        style,
        {backgroundColor: color, width: fullWidth && '80%'},
      ]}>
      <View style={styles.labelContainer}>
        <Icons variant="FontAwesome" icon={icon} size={22} color={iconColor} />
        <Typography variant="body3" color={COLORS.white}>
          {children}
        </Typography>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    borderRadius: 10,
    marginVertical: SIZES.margin,
    paddingVertical: SIZES.padding * 2,
    paddingHorizontal: SIZES.padding * 4,
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
  },
  labelContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default Buttons;
