import React, {useState} from 'react';
import {Modal, StyleSheet, View, TouchableOpacity} from 'react-native';

import {COLORS, SIZES} from '../../constants';

import {Typography, Icons} from '../atoms';

const Modals = ({modalVisible, data}) => {
  const [visible, setVisible] = useState(modalVisible);

  const renderData = () => {
    return data.map((text, index) => (
      <TouchableOpacity
        key={text.id}
        style={[styles.child]}
        onPress={() => setVisible(false)}>
        <Typography variant="body3" align="left">
          {text.value}
        </Typography>
      </TouchableOpacity>
    ));
  };

  return (
    <View>
      <View style={styles.centeredView}>
        <Modal animationType="slide" transparent={true} visible={visible}>
          <View style={styles.centeredView}>
            <View style={styles.modalView}>{renderData()}</View>
          </View>
        </Modal>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    width: '80%',
    backgroundColor: 'white',
    borderRadius: 10,
    paddingVertical: SIZES.padding * 3,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 3,
  },
  child: {
    paddingVertical: SIZES.padding * 2,
    paddingHorizontal: SIZES.padding * 4,
    borderBottomColor: COLORS.darkGray,
  },
});

export default Modals;
