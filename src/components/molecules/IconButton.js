import React from 'react';
import {TouchableOpacity, StyleSheet, View} from 'react-native';

import {COLORS, SIZES} from '../../constants';
import {Typography, Icons} from '../atoms';

const IconButton = props => {
  const {onPress, icon, iconColor} = props;

  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <Icons
        variant="FontAwesome"
        icon={icon}
        size={20}
        color={iconColor}
        style={styles.icon}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'center',
  },
  icon: {
    backgroundColor: COLORS.primary,
    padding: SIZES.padding * 2,
    borderRadius: 50,
  },
});

export default IconButton;
