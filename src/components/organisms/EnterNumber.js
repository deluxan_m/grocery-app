import React from 'react';
import {View, StyleSheet} from 'react-native';

import {SIZES} from '../../constants';

import {Typography, Input} from '../atoms';

const EnterNumber = ({
  icon,
  title,
  label,
  keyboard,
  value,
  onChange,
  secured,
}) => {
  return (
    <View style={styles.container}>
      <Typography variant="body1" align="left">
        {title}
      </Typography>
      <Input
        icon={icon}
        label={label}
        value={value}
        keyboard={keyboard}
        onChange={onChange}
        focused
        secured={secured}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: SIZES.margin * 4,
  },
});

export default EnterNumber;
