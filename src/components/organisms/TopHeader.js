import React from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';

import {SIZES} from '../../constants';

import {Icons} from '../atoms';

const TopHeader = props => {
  const {
    leftIcon,
    leftIconSize,
    leftIconPress,
    rightIcon,
    rightIconSize,
    rightIconPress,
    children,
  } = props;

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.left} onPress={leftIconPress}>
        <Icons variant="Material" icon={leftIcon} size={leftIconSize} />
      </TouchableOpacity>
      {children}
      <TouchableOpacity style={styles.right} onPress={rightIconPress}>
        <Icons variant="Material" icon={rightIcon} size={rightIconSize} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 120,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: SIZES.margin * 2,
  },
  left: {
    justifyContent: 'flex-start',
  },
  right: {
    justifyContent: 'flex-end',
  },
});

export default TopHeader;
