import React from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';

import {COLORS, SIZES} from '../../constants';

import {Typography} from '../atoms';
import {IconButton} from '../molecules';

const Next = ({nextIcon, nextIconColor, nextText, onNextPress}) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.left}>
        <Typography color={COLORS.primary}>{nextText}</Typography>
      </TouchableOpacity>
      <View style={styles.right}>
        <IconButton
          icon={nextIcon}
          iconColor={nextIconColor}
          onPress={onNextPress}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: SIZES.margin * 4,
    marginTop: SIZES.margin * 20,
  },
  left: {
    justifyContent: 'flex-start',
  },
  right: {
    justifyContent: 'flex-end',
  },
});

export default Next;
