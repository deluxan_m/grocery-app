import React from 'react';
import {View, StyleSheet} from 'react-native';

import {COLORS} from '../../constants';

import {ImageHeader} from '../molecules';

const SigninTemplate = props => {
  const {headerImage, title, subtitle} = props;

  return (
    <View style={styles.container}>
      <ImageHeader image={headerImage} title={title} subtitle={subtitle} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.white,
  },
});

export default SigninTemplate;
