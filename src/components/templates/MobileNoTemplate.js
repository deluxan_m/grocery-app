import React from 'react';
import {View, StyleSheet} from 'react-native';

import {TopHeader, EnterNumber, Next} from '../organisms';

const MobileNoTemplate = props => {
  const {
    leftIcon,
    leftIconSize,
    leftIconPress,
    rightIcon,
    rightIconSize,
    rightIconPress,
    icon,
    keyboard,
    title,
    label,
    value,
    onChange,
    secured,
    nextIcon,
    nextIconColor,
    nextText,
    onNextPress,
    children,
  } = props;
  return (
    <View style={styles.container}>
      <TopHeader
        leftIcon={leftIcon}
        leftIconSize={leftIconSize}
        leftIconPress={leftIconPress}
        rightIcon={rightIcon}
        rightIconSize={rightIconSize}
        rightIconPress={rightIconPress}>
        {children}
      </TopHeader>
      <EnterNumber
        icon={icon}
        keyboard={keyboard}
        title={title}
        label={label}
        value={value}
        onChange={onChange}
        secured={secured}
      />
      <Next
        nextIcon={nextIcon}
        nextIconColor={nextIconColor}
        nextText={nextText}
        onNextPress={onNextPress}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default MobileNoTemplate;
