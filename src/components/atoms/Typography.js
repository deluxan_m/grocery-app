import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Typography = props => {
  const {
    children,
    variant,
    family,
    color,
    align,
    marginTop,
    marginBottom,
    marginVertical,
    paddingHorizontal,
  } = props;

  const getFontSize = () => {
    switch (variant) {
      case 'h1':
        return 40;
      case 'h2':
        return 38;
      case 'h3':
        return 36;
      case 'h4':
        return 32;
      case 'h5':
        return 30;
      case 'h6':
        return 28;
      case 'body1':
        return 20;
      case 'body2':
        return 18;
      case 'body3':
        return 18;
      default:
        return 14;
    }
  };

  const getFontFamily = () => {
    switch (family) {
      case 'light':
        return 'ProductSans-Light';
      case 'light-italic':
        return 'ProductSans-LightItalic';
      case 'bold':
        return 'ProductSans-Bold';
      case 'italic':
        return 'ProductSans-Italic';
      case 'bold-italic':
        return 'ProductSans-BoldItalic';
      default:
        return 'ProductSans-Regular';
    }
  };

  const getFontAlign = () => {
    switch (align) {
      case 'left':
        return 'flex-start';
      case 'right':
        return 'flex-end';
      default:
        return 'center';
    }
  };

  return (
    <View
      style={[
        styles.container,
        {
          alignItems: getFontAlign(),
          marginTop,
          marginBottom,
          marginVertical,
          paddingHorizontal,
        },
      ]}>
      <Text
        style={[
          {
            fontSize: getFontSize(),
            fontFamily: getFontFamily(),
            color,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
  },
  typo: {
    fontFamily: 'ProductSans-Regular',
  },
});

export default Typography;
