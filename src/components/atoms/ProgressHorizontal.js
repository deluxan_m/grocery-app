import React from 'react';
import {ProgressBarAndroid} from 'react-native';

const ProgressHorizontal = ({progress}) => {
  return (
    <ProgressBarAndroid
      styleAttr="Horizontal"
      indeterminate={false}
      color="#FFFFFF"
      progress={progress}
    />
  );
};

export default ProgressHorizontal;
