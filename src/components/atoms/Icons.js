import React from 'react';

import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import EvilIcon from 'react-native-vector-icons/EvilIcons';
import FeatherIcon from 'react-native-vector-icons/Feather';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import FontAwesome5ProIcon from 'react-native-vector-icons/FontAwesome5Pro';
import FontistoIcon from 'react-native-vector-icons/Fontisto';
import FoundationIcon from 'react-native-vector-icons/Foundation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Octicon from 'react-native-vector-icons/Octicons';
import SimpleLineIcon from 'react-native-vector-icons/SimpleLineIcons';
import ZocialIcon from 'react-native-vector-icons/Zocial';

const Icons = ({variant, icon, size, color, style}) => {
  const iconProps = {
    name: icon,
    size,
    color,
    style,
  };

  switch (variant) {
    case 'AntDesign':
      return <AntDesignIcon {...iconProps} />;
    case 'FontAwesome':
      return <FontAwesomeIcon {...iconProps} />;
    case 'Material':
      return <MaterialIcon {...iconProps} />;
    case 'FontAwesome5':
      return <FontAwesome5Icon {...iconProps} />;
  }
};

export default Icons;
