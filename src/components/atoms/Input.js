import React from 'react';
import {View, Image, TextInput, StyleSheet} from 'react-native';

import {COLORS, SIZES, IMAGES} from '../../constants';

import {Typography, Icons} from '.';

const Input = ({icon, label, value, keyboard, onChange, focused, secured}) => {
  return (
    <>
      <Typography marginTop={SIZES.margin * 4} align="left">
        {label}
      </Typography>
      <View style={styles.container}>
        <Icons
          variant="FontAwesome"
          icon={icon}
          size={24}
          color={COLORS.primary}
          style={styles.icon}
        />
        <TextInput
          value={value}
          secureTextEntry={secured}
          keyboardType={keyboard}
          autoFocus={focused}
          style={styles.input}
          onChangeText={onChange}
          secured={secured}
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    paddingRight: SIZES.padding,
  },
  input: {
    borderBottomColor: COLORS.darkGray,
    borderBottomWidth: 1,
    flex: 1,
    color: COLORS.primary,
    fontSize: 20,
  },
});

export default Input;
