import React from 'react';
import {ImageBackground, View, StatusBar, StyleSheet} from 'react-native';

import {COLORS, SIZES, IMAGES} from '../constants';

import {Typography, Icons} from '../components/atoms';
import {Buttons} from '../components/molecules';

const OnBoarding = ({navigation}) => {
  const handleGetStart = () => navigation.navigate('Social');

  return (
    <>
      <StatusBar translucent backgroundColor="transparent" />
      <ImageBackground
        source={IMAGES.background1}
        style={styles.container}
        blurRadius={3}>
        <View style={styles.welcomeContainer}>
          <Icons
            variant="FontAwesome5"
            icon="carrot"
            size={50}
            color={COLORS.white}
            style={styles.brandIcon}
          />
          <Typography variant="h1" family="bold" color={COLORS.white}>
            Welcome
          </Typography>
          <Typography variant="h1" family="bold" color={COLORS.white}>
            to our store
          </Typography>
          <Typography color={COLORS.lightGray}>
            Get your groceries as fast as one hour
          </Typography>
        </View>
        <Buttons
          onPress={handleGetStart}
          title="Get Started"
          color={COLORS.primary}
          fullWidth>
          Get Started
        </Buttons>
      </ImageBackground>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  welcomeContainer: {
    width: '100%',
    marginTop: SIZES.margin * 80,
    marginBottom: SIZES.margin * 10,
    alignItems: 'center',
  },
  brandIcon: {
    marginBottom: SIZES.margin * 4,
  },
});

export default OnBoarding;
