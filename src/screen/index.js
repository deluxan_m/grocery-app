import Splash from './Splash';
import OnBoarding from './OnBoarding';
import SocialSignIn from './SocialSignin';
import MobileNumber from './MobileNumber';
import EnterOtp from './EnterOtp';
import Login from './Login';
import SelectLocation from './SelectLocation';
import Signup from './Signup';

export {
  Splash,
  OnBoarding,
  SocialSignIn,
  MobileNumber,
  EnterOtp,
  Login,
  SelectLocation,
  Signup,
};
