import React from 'react';

import {IMAGES} from '../constants';

import {SigninTemplate} from '../components/templates';

const Login = () => {
  return (
    <SigninTemplate
      headerImage={IMAGES.logo}
      title="Login"
      subtitle="Enter your email and password"
    />
  );
};

export default Login;
