import React from 'react';
import {View, StatusBar, Image, StyleSheet} from 'react-native';

import {COLORS, SIZES, IMAGES} from '../constants';

import {Typography} from '../components/atoms';
import {Buttons} from '../components/molecules';

const SocialSignIn = ({navigation}) => {
  const handleMobile = () => navigation.navigate('Mobile');
  const handleGoogle = () => console.log('Google login');
  const handleFacebook = () => console.log('Facebook login');
  return (
    <>
      <StatusBar backgroundColor="transparent" barStyle="dark-content" />
      <Image source={IMAGES.background2} />
      <View style={styles.container}>
        <Typography variant="h6" align="left">
          Get your groceries
        </Typography>
        <Typography variant="h6" align="left">
          with green
        </Typography>
      </View>
      <View style={styles.btnContainer}>
        <Buttons color={COLORS.primary} fullwidth onPress={handleMobile}>
          Connect with Mobile
        </Buttons>
        <Typography marginVertical={SIZES.margin * 4}>
          --------------Or connect with social media--------------
        </Typography>
        <Buttons
          color={COLORS.google}
          icon="google"
          iconColor={COLORS.white}
          fullwidth
          onPress={handleGoogle}>
          Connect with google
        </Buttons>
        <Buttons
          color={COLORS.facebook}
          icon="facebook"
          iconColor={COLORS.white}
          fullwidth
          onPress={handleFacebook}>
          Connect with facebook
        </Buttons>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: SIZES.margin * 2,
    marginTop: SIZES.margin * 2,
  },
  btnContainer: {
    marginHorizontal: SIZES.margin * 2,
    marginTop: SIZES.margin * 20,
  },
});

export default SocialSignIn;
