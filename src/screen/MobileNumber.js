import React, {useState} from 'react';

import {COLORS} from '../constants';

import {MobileNoTemplate} from '../components/templates';

const MobileNumber = ({navigation}) => {
  const [mobile, setMobile] = useState('+94 ');

  const handleLeftIcon = () => navigation.goBack();

  const handleChange = text => setMobile(text);

  const handleNextIcon = () => navigation.navigate('Otp');

  return (
    <MobileNoTemplate
      leftIcon="keyboard-arrow-left"
      leftIconSize={34}
      leftIconPress={handleLeftIcon}
      title="Enter your Mobile Number"
      label="Mobile Number"
      value={mobile}
      mobileNo=""
      icon="mobile"
      keyboard="phone-pad"
      onChange={text => handleChange(text)}
      nextIcon="arrow-right"
      nextIconColor={COLORS.white}
      onNextPress={handleNextIcon}
    />
  );
};

export default MobileNumber;
