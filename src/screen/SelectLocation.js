import React from 'react';
import {View, Image, StyleSheet} from 'react-native';

import {COLORS, IMAGES, SIZES} from '../constants';

import {Typography, Icons} from '../components/atoms';
import {Buttons, Picker} from '../components/molecules';
import {TopHeader} from '../components/organisms';

const SelectLocation = ({navigation}) => {
  const handleLeftIcon = () => navigation.goBack();

  const handleSubmit = () => navigation.navigate('Login');

  const provinces = [
    {id: 1, value: 'Central Province'},
    {id: 2, value: 'Eastern Province'},
    {id: 3, value: 'Northern Province'},
    {id: 4, value: 'Southern Province'},
    {id: 5, value: 'North Western Province'},
    {id: 6, value: 'North Central Province'},
    {id: 7, value: 'Uva Province'},
    {id: 8, value: 'Sabaragamuwa Province'},
  ];

  const cities = [
    {id: 1, value: 'Jaffna'},
    {id: 2, value: 'Mannar'},
    {id: 3, value: 'Vavuniya'},
  ];

  return (
    <View style={styles.container}>
      <TopHeader
        leftIcon="keyboard-arrow-left"
        leftIconSize={34}
        leftIconPress={handleLeftIcon}
      />
      <View style={styles.image}>
        <Image source={IMAGES.map} />
      </View>
      <Typography variant="h6" marginTop={SIZES.margin * 4}>
        Select Your Location
      </Typography>
      <Typography color={COLORS.darkGray}>
        Switch on your location to stay in tune with
      </Typography>
      <Typography color={COLORS.darkGray}>
        what's happening in your area
      </Typography>
      <View style={styles.pickers}>
        <Picker label="Province" topic="Choose province" data={provinces} />
        <Picker label="City" topic="Choose city" data={cities} />
      </View>
      <Buttons
        color={COLORS.primary}
        style={styles.submit}
        onPress={handleSubmit}>
        Submit
      </Buttons>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.white,
    flex: 1,
  },
  image: {
    alignItems: 'center',
  },
  pickers: {
    marginTop: SIZES.margin * 12,
  },
  submit: {
    marginHorizontal: SIZES.margin * 4,
  },
});

export default SelectLocation;
