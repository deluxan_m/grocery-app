import React, {useState} from 'react';

import {COLORS} from '../constants';

import {MobileNoTemplate} from '../components/templates';

const EnterOtp = ({navigation}) => {
  const [otp, setOtp] = useState();

  const handleLeftIcon = () => navigation.goBack();

  const handleChange = text => setOtp(text);

  const handleNextIcon = () => navigation.navigate('SelectLocation');

  return (
    <MobileNoTemplate
      leftIcon="keyboard-arrow-left"
      leftIconSize={34}
      leftIconPress={handleLeftIcon}
      title="Enter your 4 digit code"
      label="code"
      value={otp}
      mobileNo=""
      icon="mobile"
      keyboard="phone-pad"
      onChange={text => handleChange(text)}
      nextIcon="arrow-right"
      nextIconColor={COLORS.white}
      nextText="Resend code"
      onNextPress={handleNextIcon}
    />
  );
};

export default EnterOtp;
