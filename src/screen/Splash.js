import React, {useState, useEffect} from 'react';
import {View, Text, StatusBar, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

import {COLORS, SIZES} from '../constants';
import {ProgressHorizontal} from '../components/atoms';

const Splash = props => {
  const runProgress = () => {
    setTimeout(() => {
      props.navigation.navigate('OnBoarding');
    }, 2000);
  };

  useEffect(() => {
    runProgress();
  }, []);

  return (
    <>
      <StatusBar backgroundColor={COLORS.primary} />
      <View style={styles.container}>
        <View style={styles.iconContainer}>
          <Icon name="carrot" size={50} color={COLORS.white} />
        </View>
        <View style={styles.brandContainer}>
          <Text style={styles.title}>green</Text>
          <Text style={styles.subtitle}>online grocery</Text>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.primary,
  },
  iconContainer: {
    // flex: 2,
  },
  brandContainer: {
    // flex: 8,
    marginLeft: 10,
    alignItems: 'center',
  },
  title: {
    fontFamily: 'ProductSans-Bold',
    fontSize: 40,
    color: COLORS.white,
  },
  subtitle: {
    fontFamily: 'ProductSans-Regular',
    fontSize: 12,
    color: COLORS.white,
    marginTop: -SIZES.margin * 2,
    marginLeft: SIZES.margin * 4,
  },
});

export default Splash;
