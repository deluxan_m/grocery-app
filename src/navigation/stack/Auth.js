import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import {
  Splash,
  OnBoarding,
  SocialSignIn,
  MobileNumber,
  EnterOtp,
  SelectLocation,
  Login,
  Signup,
} from '../../screen';

const Stack = createStackNavigator();

const Auth = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Splash" component={Splash} />
      <Stack.Screen name="OnBoarding" component={OnBoarding} />
      <Stack.Screen name="Social" component={SocialSignIn} />
      <Stack.Screen name="Mobile" component={MobileNumber} />
      <Stack.Screen name="Otp" component={EnterOtp} />
      <Stack.Screen name="SelectLocation" component={SelectLocation} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Signup" component={Signup} />
    </Stack.Navigator>
  );
};

export default Auth;
