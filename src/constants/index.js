import {COLORS} from './colors';
import {SIZES} from './sizes';
import {IMAGES} from './images';

export {COLORS, SIZES, IMAGES};
