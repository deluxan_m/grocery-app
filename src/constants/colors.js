export const COLORS = {
  primary: '#53B175',
  white: '#FFFFFF',
  lightGray: '#FCFCFC',
  darkGray: '#CDCDCD',
  facebook: '#3b5998',
  google: '#4285F4',
};
