import background1 from '../../assets/images/background1.jpg';
import background2 from '../../assets/images/background2.jpg';
import srilankaFlag from '../../assets/images/sri-lanka-flag.jpg';
import map from '../../assets/images/map.jpg';
import logo from '../../assets/images/logo.jpg';

export const IMAGES = {
  background1,
  background2,
  srilankaFlag,
  map,
  logo,
};
